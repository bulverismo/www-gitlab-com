---
layout: handbook-page-toc
title: FY22-Q3 Learning & Development Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Welcome to the GitLab Learning & Development (L&D) newsletter! The purpose of the L&D newsletter is to enable a culture of curiosity and continuous learning that *prioritizes learning* with a [growth mindset](/handbook/values/#growth-mindset) for team members. The quarterly newsletter will raise awareness of what learning initiatives took place in the past quarter, insight into what's coming next, learning tips, and encourage participation. We will also feature leadership and learner profiles that highlight what our community has done to learn new skills. Consider this a forum to hear from others across GitLab on what learning has done for them. 

You can find more information on the [structure and process](/handbook/people-group/learning-and-development/newsletter/) for the L&D newsletter, as well as links to [past L&D Newsletters](/handbook/people-group/learning-and-development/newsletter/#past-newsletters). 

## Skill of the Month Spotlight 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/FAi4BU0ZeCU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

In August we are hosting a focused skill of the month around Career Development. 

What you can expect this month: 

* Videos each week to learn more about team members career development. 
* Live Learning sessions to learn more about our Individual Growth Plan, Having a Career Conversation with Your Manager, and Having a Career Conversation with your Direct Reports 
* Asynchronous course recommendations to help you in your career develpment 

Want to learn more and follow along this month? Check out and follow the [FY22 Skill of the Month Channel](https://gitlab.edcast.com/channel/skill-of-the-month-fy22) in GitLab Learn. 

## Learn from Leadership 

This month's Learn from Leadership looks a little different than [previous months](/handbook/people-group/learning-and-development/newsletter/#past-newsletters). August 2021 is our [Career Development](/handbook/people-group/learning-and-development/career-development/) Skill of the Month, so each week we will be releasing videos from 2 team members at GitLab discussing Career Development. This week we are learning from Pattie Egan, Vice President, People Operations, Technology & Analytics, and Darren Murph, Head of Remote. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/k8k1xYtlo6U" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

Pattie has been with GitLab since March 2021 (2021-03). Learn more about her career development in the video above. Check out the [corresponding pathway](https://gitlab.edcast.com/pathways/career-development-with-pattie-egan) in GitLab Learn to see the video and add questions for Pattie to her Async AMA Issue. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/J9n0F22d0eU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

Darren has been with GitLab since July 2019 (2019-07) as our Head of Remote. Learn more about his career development in the video above. Check out the [corresponding pathway](https://gitlab.edcast.com/pathways/career-development-with-darren-murph) in GitLab Learn to see the video and add questions for Darren to his Async AMA Issue. 

## Recap of Q2

* [FY22-Q2 Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q2/) 
* [John Fitch - Your Rest Ethic is as Important as your Work Ethic](/company/culture/all-remote/mental-health/#your-rest-ethic-is-as-important-as-your-work-ethic)
* [CEO Handbook Learning Sessions](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions)
* [Women at GitLab Mentorship Program](/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/)

Past Live Learning sessions can be found on our [Learning & Development Page](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning). 


## Upcoming in Q3 

* Skill of the Month
   * August - Career Development 
   * September - Manager of One 
   * October - Emotional Intelligence
* Crucial Conversations - October 
   * If you are interested, [sign up here](https://docs.google.com/forms/d/e/1FAIpQLScFLrxgWNjS8Vc3QeNgZ7DKvU8HIRxZ8FQdb0TB12VxJ2HCVQ/viewform). 


## Career Develpment 

Throughout the month we will be covering a variety of different topics. A great place to get started is filling out your Individual Growth Plan and setting up an Epic to track career develpment as well as an Achievement Tracker. 

Week 2 in August will be all about setting up your career development. Check out the videos below to learn more. 

### Epics for Tracking Career Development 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/xuisSgBQtaU" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

You can learn more in the [Epics for Career Development handbook section](https://about.gitlab.com/handbook/people-group/learning-and-development/career-development/#use-gitlab-epics-to-track-your-career-development). 

### Individual Growth Plan 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/0ih0lMoKxN0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

You can learn more in the [Individual Growth Plan handbook section](/handbook/people-group/learning-and-development/career-development/#individual-growth-plan). 

### Achievement Tracker 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/p4a60slPg5k" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

You can learn more in the [Achievement Tracker handbook section](/handbook/people-group/learning-and-development/career-development/#tracking-your-accomplishments). 

## Department Spotlight 

This month we are highlighting a project the Diversity, Inclusion, & Belonging (DIB) Team is working on around how everything connects. 

- Can you tell us a little bit about the DIB Story project you have been working on? 
   - Liam: We are creating a piece which will live in the handbook to show the breadth that DIB has and can ultimately have. We have had some amazing success stories both within the direct DIB team but with team members who truly become extensions of the DIB Team. DIB is everyone's job  and the story shows how this is applicable. 
- What steps can we take to work towards the DIB Story as a company? 
   - Liam: I think understanding your role in DIB at GitLab is important and that’s what this will hopefully help with. Being open to learning more about DIB and taking the actions you are comfortable with to help move the needle. Whether thats ensuring you have more coffee chats to ensure you are bringing in diverse perspectives into your work or leading a TMRG, these actions all help achieve the ultimate goal of creating an environment where everyone belongs. 
- What is the ultimate goal of implementing the DIB Story?  
   - Liam: That people will see how a spark of an idea can influence Diversity, Inclusion and Belonging. To show the organisation how big the DIB team is when everyone gets involved . To encourage people to use there voice in what way they are comfortable to help influence the DIB Journey. 
- Are there resources where team members can learn more about this? 
   - Liam: This will soon be a handbook page but also the [DIB page](/company/culture/inclusion/) has lots of information on how you can get involved more with DIB at Gitlab. 


## Learning at GitLab  

When it comes to learning, maybe you aren't sure where to start. Here are some great options: 

* Set aside 2 hours per week to work on personal development
* Check out the [Certified Associate for team members](https://gitlab.edcast.com/pathways/copy-of-gitlab-certified-associate-pathway) training
* Take a look at our [Recommended LinkedIn Learning Courses Channel](https://gitlab.edcast.com/channel/recommended-linkedin-learning-courses). You will find courses recommended by our L&D Team as well as your GitLab Team Member peers. 
* We want to know more about how you prefer to learn. Please take [this short survey](https://forms.gle/aoojWtLwRoTrqa3d9) so we can better meet the needs of your learning style. 


### Team Member Contributions 

We love that our team members have been contributing to GitLab Learn by making courses and putting together learning hubs. If you want to contribute as well, take our Learning Evangelist Pathway. 

1. [Technical Writing Fundamentals](https://gitlab.edcast.com/pathways/ECL-02528ee2-c334-4e16-abf3-e9d8b8260de4)
1. [Marketing Learning Hub and GitLab Marketing Badges](https://gitlab.edcast.com/channel/gitlab-marketing-learning-hub)


## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter/-/issues/12). 
