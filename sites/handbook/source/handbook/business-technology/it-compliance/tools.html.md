---
layout: handbook-page-toc
title: "IT Compliance and Security Tools"
description: "IT Compliance and Security"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}



### Tools IT Security and Compliance Use

These are the tools currently managed and in use by the IT Compliance and Security teams. For access or questions, please reach out in the #it_compliance_security_help Slack channel.


| Tool                         | Description                                                                                     |      User Guide     |  About  |
| :---------------------------- | :---------------------------------------------------------------------------------------------- | :--------------------: | :----: |
| Nira                   | Nira is a real-time access control system that provides visibility and management over who has access to company documents in Google Workspace.                                                                        | [Nira End User Guide](sites/handbook/source/handbook/business-technology/it-compliance/tools/nira/nira-guide.html.md) | [Nira](sites/handbook/source/handbook/business-technology/it-compliance/tools/nira/nira.html.md)    |
| NordLayer                       | NordLayer is our supported VPN (Virtual Private Network) platform for GitLab Team Members.              | [NordLayer End User Guide](sites/handbook/source/handbook/business-technology/it-compliance/tools/NordLayer/nordlayer-guide.html.md) | [NordLayer](sites/handbook/source/handbook/business-technology/it-compliance/tools/NordLayer/nordlayer.html.md) |

