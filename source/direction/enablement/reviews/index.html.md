---
layout: markdown_page
title: Enablement Direction Page reviews
description: "The bi-weekly direction review schedule."
canonical_path: "/direction/enablement/reviews/"
---

## What's on this page

This page contains the bi-weekly Enablement & SaaS Platforms section direction review. Every other week, the PM teams reviews one of the groups direction pages asynchronously and adds their questions to the weekly PM agenda discussion. During the weekly meeting, these questions are discussed synchronously and recorded separate from the main meeting.

### Schedule

| Group | Date | PM | Recording |
| ----- | ---- | -- | --------- |
| TBD | 2022-06-01 | TBD | N/A |
| TBD | 2022-06-15 | TBD | N/A |